var nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');
var fs = require('fs');

var transport = nodemailer.createTransport(smtpTransport({
    service: 'gmail',
    auth: {
        user: 'smartbookingbot@gmail.com',
        pass: 'asdf486.2'
    }
}));

// setup e-mail data with unicode symbols
//var htmlstream = fs.createReadStream('mail-templates/basic.html');
var mailOptions = {
    from: '"Rezervační robot" <robot@petrbily.com>', // sender address
    to: 'petr.bily@gmail.com, Jirkuv@email.cz', // list of receivers
    subject: 'Rezervace', // Subject line
    text: 'Ahoj,\n jsem rezervační robot. Testuji odesílání mailů o rezervacích. Pokud jsi zprávu dostal, funguje to. Na zprávu neodpovídej, nečtu je, jen odesílám :-).\n\nRobot'
};

// send mail with defined transport object
transport.sendMail(mailOptions, function(error, info){
    if(error){
        return console.log(error);
    }
    console.log('Message sent: ' + info.response);
});