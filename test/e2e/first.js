'use strict';

const assert = require('assert');
const moment = require('moment');
moment.locale('cs');

const URL = 'http://hodiny.hamrsport.cz/';
const LOGIN_ID = '#ctl00_toolboxRight_tbLoginUserName';
const PASSWORD_ID = '#ctl00_toolboxRight_tbLoginPassword';
const LOGIN = 'pbily';
const PASSWORD = 'saxophonist1';

const LOCALITY_ID = '#ctl00_workspace_ddlLocality'; //ID selectu pro vyber lokality
const LOCALITY_INDEX = '171'; //lokalita 171 Branik

const SPORT_ID = '#ctl00_workspace_ddlSport'; //ID selectu pro vyber sportu
const SPORT_INDEX = '140'; //sport 140 Badminton

const UTERY_INDEX = 2; //sledovany den
const SLOT_START_INDEX = 20; //17:00
const SLOT_STOP_INDEX = 26; //20:00

describe('HAMR badminton rezervace', () => {
    it('HAMR badminton test', () => {
        browser.url(URL);
        browser.setValue(LOGIN_ID, 'pbily');
        browser.setValue(PASSWORD_ID, 'saxophonist1');
        browser.click('#ctl00_toolboxRight_btLogin');

        var selectLocality = $(LOCALITY_ID);
        selectLocality.selectByValue(LOCALITY_INDEX);

        var selectSport = $(SPORT_ID);
        selectSport.selectByValue(SPORT_INDEX);

        console.log("lokalita: " + selectLocality.getValue());
        console.log("sport: " + selectSport.getValue());

        //rgDL_0 az rgDL_14
        var selectedDayIndex;
        for (var i = 0; i <= 14; i++) {
            var tmpDay = $('#rgDL_' + i);
            var tmpMomentDay = moment(tmpDay.getText(), 'dd DD.MM.YYYY', 'cs');

            if (tmpMomentDay.day() === UTERY_INDEX) {
                selectedDayIndex = i;
                break;
            }
        }
        console.log(selectedDayIndex);
        //rgI_4_20

        console.log("finished");
        //TODO zparsovat tabulku
        //TODO moznosti rezervace
    })
});