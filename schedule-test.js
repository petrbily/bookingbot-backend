'use strict';

const moment = require('moment');
const schedule = require('node-schedule');

moment.locale('cs');

var rule = new schedule.RecurrenceRule();
rule.second = new schedule.Range(0, 59, 5);

var job = schedule.scheduleJob(rule, function(){
    console.log('5 second job');
});